var _ = require("lodash"),
	path = require("path");

function getCommand(cmdChar, cmd, commands) {
	var module = require(path.join(__dirname, cmd + ".js"));
	var aliases = _.pluck(_.where(commands, {"command": cmd}), "aliases");
	return cmdChar + cmd + (typeof aliases[0] !== "undefined" ? " [" + aliases.join(", ") + "]" : "") + " (" + module.use + "): " + cmdChar + cmd + (module.args === 0 || module.usage.length < 1 ? "" : " <" + module.usage.join("> <") + ">") + (module.args === 0 || module.optional.length < 1 ? "" : " [" + module.optional.join("] [") + "]");
}

exports.main = function(slack, profile, cmdChar, command, masters, commands, channel, user, type, im, args) {
	var matchedAlias = _.pluck(_.where(commands, {aliases: [args[0]]}), "command");
	if(matchedAlias.length > 0) args[0] = matchedAlias[0];
	if(args.length == 1 && _.where(commands, {"command": args[0]}).length > 0) {
		channel.send("[" + _.capitalize(command) + "] " + getCommand(cmdChar, args[0], commands));
	} else {
		var cmdHelp = "\n>>>";
		commands.forEach(function(cmd) {
			cmd = cmd.command;
			cmdHelp += "\n" + getCommand(cmdChar, cmd, commands);
		});
		channel.send("[" + _.capitalize(command) + "] You can use the following commands: " + cmdHelp);
	}
};

exports.args = [0, 1];
exports.usage = [];
exports.optional = ["command"];
exports.use = "Shows help";
