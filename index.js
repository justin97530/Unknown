var Slack = require("slack-client"),
	_ = require("lodash"),
	util = require("util"),
	path = require("path"),
	toolkit = require("lumios-toolkit"),
	pmx = require("pmx"),
	keys = require(path.join(__dirname, "keys.js")),
	slack = new Slack(keys.slack, true, true);

var masters = ["U0AJCH48J", "U07S6KYDR"],
	commands = [{"command": "help", "aliases": ["h"]}, {"command": "test", "aliases": ["t"]}],
	cmdChar = "!";

slack.on("loggedIn", function() {
	toolkit.info("Logged into Slack.");
});

slack.on("open", function() {
	toolkit.success("Connected to Slack.");
});

slack.on("close", function() {
	toolkit.warn("Disconnected from Slack");
});

slack.on("error", function(error) {
	toolkit.error("Error: " + util.inspect(error));
});

slack.on("presenceChange", function(user, presence) {
	toolkit.info("Presence Changed: " + user.name + " to " + presence);
});

slack.on("userTyping", function(user, channel) {
	toolkit.info("User Typing: " + user.name + " in " + channel.name);
});

slack.on("userChange", function(user) {
	toolkit.info("User Change: " + user.name);
});

slack.on("message", function(data) {
	if(typeof data.subtype !== "undefined" && ["me_message", "message_changed"].indexOf(data.subtype) === -1) return;
	var isMessage = data.subtype !== "message_changed",
		me = slack.self,
		profile = me._client.users[me.id].profile,
		channel = slack.getChannelGroupOrDMByID(data.channel),
		user = slack.getUserByID(isMessage ? data.user : data.message.user),
		text = isMessage ? data.text : data.message.text,
		type = typeof data.subtype === "undefined" ? data.type : data.subtype,
		im = channel.is_im === true ? true : false;
	if(text.length < 1) return;
	toolkit.info("Message: [" + channel.name + "]<" + type + "> " + user.name + ": " + text);
	if(text.startsWith(cmdChar) || im) {
		try {
			var args = text.split(" "),
				command = (text.startsWith(cmdChar) ? args.splice(0, 1)[0].slice(cmdChar.length) : args.splice(0, 1)[0]).toLowerCase(),
				matchedAlias = _.pluck(_.where(commands, {aliases: [command]}), "command");
			if(matchedAlias.length > 0) command = matchedAlias[0];
			if(_.where(commands, {command: command}).length < 1) return;
			var module = require(path.join(__dirname, "commands", command + ".js"));
			if((module.args === -1 && args.length > 0)  || module.args === args.length || (Array.isArray(module.args) && module.args.indexOf(args.length) != -1)) module.main(slack, profile, cmdChar, command, masters, commands, channel, user, type, im, args);
			else require(path.join(__dirname, "commands", "help.js")).main(slack, profile, cmdChar, "help", masters, commands, channel, user, type, im, [command]);
			toolkit.debug("Command: [" + channel.name + "] " + user.name + ": " + command + " | " + args.join(" "));
		} catch(e) {
			channel.send("Failed to run command " + command + ". Here's what I know: ```" + e + "```");
		}
	}
});

slack.login();
